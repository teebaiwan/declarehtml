# declarehtml

## 简介

在做很多同步页面的时候，使用mvvm框架+重构建工具体验很不好。使用如jquery这种库操作dom的方式又会觉得很累。我们就是想要一个在html中就能看清元素和元素之间的关系(如勾选同意checkbox启用同意按钮)，又想要有一个很好的模块化封装（仅仅js的封装，同步的页面对html不能封装）。 但通过数据驱动的方式做是不现实的（因为最终形成的html必须在js执行完成之后才行），所以还是使用操作dom的方式，我们提供全局的选择器穿插在html的属性之间，通过函数来传递状态，抽象逻辑，封装业务。由于我的电脑上只有 chrome 和 safari 所以并没有测试 ie 和firefox 等浏览器。最后 如果你有兴趣 欢迎加入 女神前端俱乐部共同探讨 群号 : 632826899

## 使用

```html
<script src="your/path/src/index.js"></script>
```
会在全局挂载一个变量 declare

## hello 

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>hello</title>
</head>
<body d-mod="main">
  <button d-click="alert('The tag\'s name is ' + $e.target.tagName)">hello</button>
  <script src="../src/index.js"></script>
  <script>
    declare.create(document.body,{})
  </script>
</body>
</html>
```
`d-mod`属性代表使用 `main`模块,`main`是自带的一个模块 里面包括 `d-click` 和一些常用的dom操作函数，这里只是简单的调用window下的alert函数 弹窗一条信息
## 特色

1. 声明式的操作dom
2. 完善的模块机制，推崇模块的方式开发 一切皆模块
3. html中可直接写选择器 到你的handler中自动转换为element对象
4. 遵循函数式风格 （尽量少的状态，函数的可组合性，尽量纯的函数）
